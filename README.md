# calendar_api

## Authors
* **Gagatek Paweł** - *Initial work* - [Github profile](https://github.com/GagatekPolice) [Gitlab profile](https://gitlab.com/MasterRudy)  [Mail to me](mailto:gagatek_police@wp.pl)


## Use
**before launch**
````
composer install
````
 **start app** - starts symfony on localhost
````
bin/start.sh
````
 **stop app** 
````
bin/stop.sh
````
 **coding standards tool** 
````
bin/php-cs-fixer.sh
````
 **all endpoints** 
````
bin/endpoints.sh
````
**migrate tables in database**
````
bin/generateTables.sh
````
**get server services**
````
bin/getServices.sh | grep service
````
**get password hash**
````
php bin/console security:encode-password
````