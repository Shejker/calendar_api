<?php

declare(strict_types=1);

namespace App\Handler;

use App\Entity\Calendar;
use App\Entity\ApiUser;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Psr\Log\LoggerInterface;

class UserHandler extends AbstractHandler
{
    public function __construct(EntityManager $entityManager, \Swift_Mailer $mailer, \Twig\Environment $templating, UserPasswordEncoderInterface $passwordEncoder, TokenStorageInterface $tokenStorage, LoggerInterface $logger)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        parent::__construct($entityManager, $passwordEncoder, $tokenStorage, $logger);
    }

    public function post(array $parameters) : Response
    {
        $this->isRequestEnable();
        $this->logger->info('Rozpoczęto dodawanie użytkownika', ['ordering_person' => $this->user->getUsername()]);

        if (!isset($parameters['name']) || !isset($parameters['email']) || !isset($parameters['password'])) {
            return new JsonResponse(['Nie zadeklarowano pól, [name, email lub password]'], Response::HTTP_BAD_REQUEST);
        }

        if (!preg_match("/^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/u", $parameters['password'])) {
            return new JsonResponse(['Podane haslo jest za slabe'], Response::HTTP_BAD_REQUEST);
        }

        $user = new ApiUser($parameters['name'], $parameters['email'], $parameters['password'], $parameters['roles'] ?? ['ROLE_USER'], $parameters['notification'] ?? false);

        $user = $this->setPasswordHash($user);


        try {
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        } catch (UniqueConstraintViolationException $exception) {
            return new JsonResponse(['Użytkownika o takiej nazwie lub adresie email już istnieje!'], Response::HTTP_CONFLICT);
        } catch (\Exception $exception) {
            return new Response('Internal server error', Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new JsonResponse($user->getUserData(), Response::HTTP_CREATED);
    }

    public function delete(int $userId) : Response
    {
        $user = $this->entityManager->getRepository(ApiUser::class)
            ->find($userId);

        if (!$user) {
            return new JsonResponse(Response::HTTP_NOT_FOUND);
        }

        $this->entityManager->remove($user);
        $this->entityManager->flush();

        return new JsonResponse(Response::HTTP_OK);
    }

    public function put(array $parameters, int $userId) : Response
    {
        $this->isRequestEnable();
        $this->logger->info('Rozpoczęto aktualizacje użytkownika', ['ordering_person' => $this->user->getUsername()]);
        $user = $this->entityManager->getRepository(ApiUser::class)->find($userId);

        if (!$user) {
            return new JsonResponse(['Nie znaleziono użytkownika'], Response::HTTP_NOT_FOUND);
        }

        if ($this->user->getId() !== $user->getId() && !in_array('ROLE_CALENDAR_ADMIN', $this->user->getRoles())) {
            return new JsonResponse(['Niedozwolna próba aktualizacji innego użytkownika'], Response::HTTP_UNAUTHORIZED);
        }

        if (isset($parameters['password'])) {
            $user->setPassword(
                $this->passwordEncoder->encodePassword(
                    $user,
                    $parameters['password']
                )
            );
        }

        if (isset($parameters['status'])) {
            if (!in_array('ROLE_CALENDAR_ADMIN', $this->user->getRoles())) {
                return new JsonResponse(['Niedozwolna próba aktualizacji użytkownika'], Response::HTTP_UNAUTHORIZED);
            }
            $user->setStatus($parameters['status']);
        }

        if (isset($parameters['notification'])) {
            $user->setNotification($parameters['notification']);
        }

        $this->entityManager->flush();

        return new Response('', Response::HTTP_OK);
    }

    public function changePassword(array $parameters, int $userId = null) : Response
    {
        $this->isRequestEnable();

        if (!isset($parameters['name']) || !isset($parameters['email'])) {
            return new JsonResponse(['Nie zadeklarowano pól, [name lub email]'], Response::HTTP_BAD_REQUEST);
        }

        if ($userId) {
            $user = $this->entityManager->getRepository(ApiUser::class)->find($userId);
        } else {
            $dql = 'SELECT user FROM App\Entity\ApiUser user WHERE user.userName LIKE \'' . $parameters['name'] . '\'';

            $queryUser = $this->entityManager->createQuery($dql)
                ->setMaxResults(1)
                ->getResult();

            $user = array_pop($queryUser);
        }


        if (!$user) {
            return new JsonResponse(['Nie znaleziono użytkownika'], Response::HTTP_NOT_FOUND);
        }

        if ((($parameters['email'] !== $user->getEmail() || $parameters['userName'] !== $user->getUserName()))) {
            return new JsonResponse(['Nieprawidłowe dane'], Response::HTTP_UNAUTHORIZED);
        }

        if (!is_string($this->user) && !in_array('ROLE_CALENDAR_ADMIN', $this->user->getRoles())) {
            return new JsonResponse(['Jedynie admin może zlecić zmianę hasła'], Response::HTTP_UNAUTHORIZED);
        }

        $password = $this->getRandomPassword();

        $message = (new \Swift_Message('Zmiana hasła'))
            ->setFrom('test@serwer2065955.home.pl')
            ->setTo($user->getEmail())
            ->setBody(
                $this->templating->render(
                    'emails/changePassword.html.twig',
                    [
                        'password' => $password,
                        'userName' => $user->getUserName()
                    ],
                ),
                'text/html'
            );

        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user,
                $password
            )
        );

        $this->mailer->send($message);
        $this->entityManager->flush();

        return new Response('', Response::HTTP_OK);
    }

    public function getRandomPassword()
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }
    public function self()
    {
        $this->isRequestEnable();
        $this->logger->info('Rozpoczęto pobranie użytkownika', ['ordering_person' => $this->user->getUsername()]);
        return new JsonResponse($this->user->getUserData());
    }

    public function getAll()
    {
        $users = $this->entityManager->getRepository(ApiUser::class)->findAll();
        foreach ($users as $user) {
            $userJson[] = $user->getUserData();
        }
        return new JsonResponse($userJson);
    }
}
