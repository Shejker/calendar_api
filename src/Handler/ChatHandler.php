<?php

declare(strict_types=1);

namespace App\Handler;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Psr\Log\LoggerInterface;
use App\Entity\Chat;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class ChatHandler extends AbstractHandler
{
    public function __construct(EntityManager $entityManager, UserPasswordEncoderInterface $passwordEncoder, TokenStorageInterface $tokenStorage, LoggerInterface $logger)
    {
        parent::__construct($entityManager, $passwordEncoder, $tokenStorage, $logger);
    }

    public function post(array $parameters) : Response
    {
        if (!isset($parameters['userName']) || !isset($parameters['message'])) {
            return new JsonResponse(['Nie zadeklarowano pól, [userName, date lub message'], Response::HTTP_BAD_REQUEST);
        } else {
            $chat = new Chat($parameters['userName'], $parameters['message'], $this->user->getId());
        }

        $this->entityManager->persist($chat);
        $this->entityManager->flush();

        return new JsonResponse($chat->getId(), Response::HTTP_CREATED);
    }

    public function get(string $count)
    {
        $dql = 'SELECT chat FROM App\Entity\Chat chat ORDER BY chat.id desc';

        $queryChat = $this->entityManager->createQuery($dql)->setMaxResults((int) $count);

        $chats =  $queryChat->getResult();

        $output = [];
        foreach ($chats as $chat) {
            $output[] = $chat->getData();
        }
        return new JsonResponse($output, Response::HTTP_OK);
    }
}
