<?php

declare(strict_types=1);

namespace App\Handler;

use App\Entity\Calendar;
use App\Entity\ApiUser;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Psr\Log\LoggerInterface;

class CalendarHandler extends AbstractHandler
{
    public function __construct(EntityManager $entityManager, UserPasswordEncoderInterface $passwordEncoder, TokenStorageInterface $tokenStorage, LoggerInterface $logger)
    {
        parent::__construct($entityManager, $passwordEncoder, $tokenStorage, $logger);
    }

    public function post(array $parameters) : Response
    {
        $calendars = $this->entityManager->getRepository(Calendar::class)->findAll();
        if ($calendars) {
            return new JsonResponse(['Obiekt kalendarza już istnieje'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        if (!isset($parameters['name']) || !isset($parameters['email']) || !isset($parameters['password']) || !isset($parameters['userName'])) {
            return new JsonResponse(['Nie zadeklarowano pól, [name, userName, email lub password].'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        if (!preg_match("/^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/u", $parameters['password'])) {
            return new JsonResponse(['Podane haslo jest za slabe'], Response::HTTP_BAD_REQUEST);
        }

        $calendar = new Calendar($parameters['name']);
        $user = new ApiUser($parameters['userName'], $parameters['email'], $parameters['password'], ['ROLE_CALENDAR_ADMIN']);

        $user = $this->setPasswordHash($user);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $calendar->setUserNameId($user->getId());
        $this->entityManager->persist($calendar);
        $this->entityManager->flush();

        return new Response('', Response::HTTP_CREATED);
    }
}
