<?php

declare(strict_types=1);

namespace App\Handler;

use App\Entity\Calendar;
use App\Entity\ApiUser;
use App\Entity\Event;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Psr\Log\LoggerInterface;

class EventHandler extends AbstractHandler
{
    public function __construct(EntityManager $entityManager, UserPasswordEncoderInterface $passwordEncoder, TokenStorageInterface $tokenStorage, LoggerInterface $logger)
    {
        parent::__construct($entityManager, $passwordEncoder, $tokenStorage, $logger);
    }

    public function post($parameters)
    {
        $this->isRequestEnable();
        $this->logger->info('Rozpoczęto dodawanie eventu', ['ordering_person' => $this->user->getUsername()]);
        if (!isset($parameters['name']) || !isset($parameters['date']) || !isset($parameters['description']) || !isset($parameters['users'])) {
            return new JsonResponse(['Nie zadeklarowano pól, [name, date, description lub users'], Response::HTTP_BAD_REQUEST);
        }

        if (is_array($parameters['users'])) {
            foreach (array_unique($parameters['users']) as $userName) {
                $user = $this->findUserByName($userName);

                if (!$user) {
                    return new JsonResponse(['message' => 'Nie znaleziono użytkownika ', 'userName' => $userName], Response::HTTP_NOT_FOUND);
                }
                $user = null;
            }
        } else {
            return new JsonResponse(['Pole users nie jest tablicą.'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $event = new Event($parameters['name'], $parameters['date'], $parameters['description'], $parameters['users'], $this->user->getId(), $parameters['priority'] ?? 0);

        if (isset($parameters['status'])) {
            $event->setStatus($parameters['status']);
        }

        $this->entityManager->persist($event);
        $this->entityManager->flush();

        return new JsonResponse($event->getData(), Response::HTTP_CREATED);
    }

    public function delete(int $eventId)
    {
        $this->isRequestEnable();
        $event = $this->entityManager->getRepository(Event::class)->find($eventId);
        $this->logger->info('Rozpoczęto usuniecie eventu', ['ordering_person' => $this->user->getUsername(), 'eventId' => $eventId]);

        if (!$event) {
            return new JsonResponse(['message' => 'Nie znaleziono eventu', 'eventId' => $eventId], Response::HTTP_NOT_FOUND);
        }

        if ($this->user->getId() === $event->getOwnerId() || in_array('ROLE_CALENDAR_ADMIN', $this->user->getRoles())) {
            $this->entityManager->remove($event);
            $this->entityManager->flush();
        } else {
            return new Response('Niedozwolna próba usuniecie eventu', Response::HTTP_UNAUTHORIZED);
        }

        return new Response('', Response::HTTP_CREATED);
    }

    public function patch(int $eventId, $parameters)
    {
        $this->isRequestEnable();

        $this->logger->info('Rozpoczęto aktualizacje eventu', ['ordering_person' => $this->user->getUsername()]);
        $event = $this->entityManager->getRepository(Event::class)->find($eventId);

        if (!$event) {
            return new JsonResponse(['Nie znaleziono eventu'], Response::HTTP_NOT_FOUND);
        }

        if ($this->user->getId() !== $event->getOwnerId() && !in_array('ROLE_CALENDAR_ADMIN', $this->user->getRoles())) {
            return new JsonResponse(['Niedozwolna próba aktualizacji eventu przez niedozwolonego użytkownika'], Response::HTTP_UNAUTHORIZED);
        }

        if (isset($parameters['name']) && !in_array('ROLE_MEMBER', $this->user->getRoles())) {
            $event->setName($parameters['name']);
        }

        if (isset($parameters['date']) && !in_array('ROLE_MEMBER', $this->user->getRoles())) {
            $event->setDate($parameters['date']);
        }

        if (isset($parameters['description']) && !in_array('ROLE_MEMBER', $this->user->getRoles())) {
            $event->setDescription($parameters['description']);
        }

        if (isset($parameters['priority'])) {
            if ($parameters['priority']<=0 || $parameters['priority']>5) {
                return new JsonResponse(['Niepoprawny priorytet! [1-5]'], Response::HTTP_BAD_REQUEST);
            }
            $event->setPriority($parameters['priority']);
        }

        if (isset($parameters['status'])) {
            $event->setStatus($parameters['status']);
        }
        if (isset($parameters['users']) && !in_array('ROLE_MEMBER', $this->user->getRoles())) {
            if (is_array($parameters['users'])) {
                foreach (array_unique($parameters['users']) as $userName) {
                    $user = $this->findUserByName($userName);

                    if (!$user) {
                        return new JsonResponse(['message' => 'Nie znaleziono użytkownika ', 'userName' => $userName], Response::HTTP_NOT_FOUND);
                    }
                    $user = null;
                };
                if (!array_search($this->user->getUsername(), $parameters['users'])) {
                    array_push($parameters['users'], $this->user->getUsername());
                }
                $event->setUsers($parameters['users']);
            } else {
                return new JsonResponse(['Pole users nie  jest tablicą.'], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        }

        $event->setUsers(array_values(array_unique($event->getUsers())));
        $this->entityManager->flush();

        return new JsonResponse($event->getData());
    }

    public function getUserEvents($date)
    {
        $this->logger->info('Rozpoczęto pobranie eventów usera', ['ordering_person' => $this->user->getUsername()]);
        $date = new \DateTime($date);
        $dateRange = $this->getDateRangeByWeek((int) $date->format("W"), (int) $date->format("Y"));

        $events = $this->findEventByNameAndDateRange($this->user->getUsername(), $dateRange['start'], $dateRange['end']);

        $output = [];
        foreach ($events as $event) {
            $output[] = $event->getData();
        }
        return new JsonResponse($output);
    }

    private function getDateRangeByWeek(int $week, int $year)
    {
        $date = new \DateTime();

        $date->setISODate($year, $week);
        $dateRange['start'] = $date->format('Y-m-d');
        $date->modify('+6 days');
        $dateRange['end'] = $date->format('Y-m-d');

        return $dateRange;
    }

    private function findEventByNameAndDateRange(string $userName, string $startDate, string $endDate)
    {
        $dql = 'SELECT event FROM App\Entity\Event event WHERE JSON_CONTAINS(event.users, \'1\') = \'' . $userName . '\' AND event.date BETWEEN \'' . $startDate . '\' AND \'' . $endDate . '\'' ;

        $queryUser = $this->entityManager->createQuery($dql);

        return $queryUser->getResult();
    }
}
