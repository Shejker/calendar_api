<?php

declare(strict_types=1);

namespace App\Handler;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Doctrine\ORM\EntityManager;
use App\Entity\ApiUser;
use Psr\Log\LoggerInterface;

abstract class AbstractHandler
{
    protected $user;

    protected $entityManager;

    protected $passwordEncoder;
    
    protected $logger;

    public function __construct(EntityManager $entityManager, UserPasswordEncoderInterface $passwordEncoder, TokenStorageInterface $tokenStorage, LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->entityManager = $entityManager;
        $this->user = $tokenStorage->getToken()->getUser();
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function setPasswordHash(ApiUser $user)
    {
        $encodedPassword = $this->passwordEncoder->encodePassword($user, $user->getPassword());
        $user->setPassword($encodedPassword);
        return $user;
    }

    public function findUserByName(string $userName)
    {
        $dql = 'SELECT user FROM App\Entity\ApiUser user WHERE user.userName LIKE \'' . $userName . '\'';

        $queryUser = $this->entityManager->createQuery($dql)
            ->setMaxResults(1)
            ->getResult();

        return array_pop($queryUser) ?? null;
    }

    private function isUserPasswordExpired()
    {
        return $this->user->getpasswordExpirationDate() >= new \DateTimeZone(date_default_timezone_get());
    }

    public function isRequestEnable()
    {
        if (!is_string($this->user) && $this->isUserPasswordExpired() && !in_array('ROLE_CALENDAR_ADMIN', $this->user->getRoles())) {
            return new Response('Zablokowano dostęp z uwagi na wygasle haslo', Response::HTTP_FORBIDDEN);
        };
        return true;
    }
}
