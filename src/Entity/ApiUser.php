<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\ApiUserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=ApiUserRepository::class)
 */
class ApiUser implements UserInterface
{
    const ROLE_CALENDAR_ADMIN = 'ROLE_CALENDAR_ADMIN';

    const ROLE_CALENDAR_USER = 'ROLE_USER';

    const ROLE_CALENDAR_MEMBER = 'ROLE_MEMBER';

    const STATUS_DISABLED = 'disabled';

    const STATUS_ENABLED = 'enabled';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true, name = "userName")
     */
    private $userName;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $status = self::STATUS_ENABLED;
    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="datetime", name = "passwordExpiration")
     */
    private $passwordExpiration;

    /**
     * @ORM\Column(type="boolean", name = "notification")
     */
    private $notification;

    public function __construct(string $userName, string $email, string $password, array $roles = ['ROLE_USER'], bool $notification = false)
    {
        $this->userName = $userName;
        $this->email = $email;
        $this->password = $password;
        $this->roles = $roles;
        $this->notification = $notification;

        $date = new \DateTime('now', new \DateTimeZone(date_default_timezone_get()));
        $this->passwordExpiration = $date->modify('+3 month');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->userName;
    }

    public function getEmail(): string
    {
        return (string) $this->email;
    }

    public function setUserName(string $userName): self
    {
        $this->userName = $userName;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status === self::STATUS_ENABLED ? self::STATUS_ENABLED : self::STATUS_DISABLED;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getNotification(): bool
    {
        return $this->notification;
    }

    public function setNotification(bool $notification): self
    {
        $this->notification = $notification;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;
        $date = new \DateTime('now', new \DateTimeZone(date_default_timezone_get()));
        $this->passwordExpiration = $date->modify('+3 month');

        return $this;
    }

    public function getpasswordExpirationDate()
    {
        return $this->passwordExpiration;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getUserData()
    {
        return [
            'id' => $this->id,
            'userName' => $this->userName,
            'email' => $this->email,
            'status' => $this->status,
            'roles' => $this->roles,
            'passwordExpiration' => $this->passwordExpiration,
            'notification' => $this->notification
        ];
    }
}
