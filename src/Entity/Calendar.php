<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name = "calendar")
 * @ORM\MappedSuperclass
 */
class Calendar
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name = "id")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=32, name = "name")
     */
    private $name;

    /**
     * @ORM\Column(type="integer", name = "userNameId")
     */
    private $userNameId;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function setUserNameId(int $userNameId)
    {
        $this->userNameId = $userNameId;
    }
}
