<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Security;

/**
 * @ORM\Entity
 * @ORM\Table(name = "event")
 * @ORM\MappedSuperclass
 */
class Event
{
    const STATUS_DISABLED = 'disabled';

    const STATUS_ENABLED = 'enabled';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer", name = "id")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=32, name = "name")
     */
    private $name;

    /**
     * @ORM\Column(type="datetime", name = "date")
     */
    private $date;

    /**
     * @ORM\Column(type="string", name = "description")
     */
    private $description;

    /**
     * @ORM\Column(type="json", name = "users")
     */
    private $users;

    /**
     * @ORM\Column(type="integer", name = "ownerId")
     */
    private $ownerId;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $status = self::STATUS_DISABLED;

    /**
     * @var Security
     */
    private $security;

    /**
     * @ORM\Column(type="integer", name = "priority")
     */
    private $priority;

    public function __construct(string $name, $date, string $description, array $users, int $ownerId, int $priority = 0)
    {
        $this->name = $name;
        $this->date = new \DateTime($date);
        $this->description = $description;
        $this->users = $users;
        $this->ownerId = $ownerId;
        $this->priority = $priority;
    }

    public function getOwnerId()
    {
        return $this->ownerId;
    }

    public function getUsers()
    {
        return $this->users;
    }

    public function getData()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'date' => $this->date,
            'description' => $this->description,
            'users' => $this->users,
            'status' => $this->status ? $this->status : self::STATUS_ENABLED,
            'owner' => $this->ownerId,
            'priority' => $this->priority
        ];
    }

    public function setDate($date)
    {
        $this->date = $this->date = new \DateTime($date);
        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function setUsers($users)
    {
        $this->users = $users;
        return $this;
    }

    public function setStatus($status)
    {
        $this->status = $status === self::STATUS_ENABLED ? self::STATUS_ENABLED : self::STATUS_DISABLED;
        return $this;
    }

    public function getPriority(): int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }
}
