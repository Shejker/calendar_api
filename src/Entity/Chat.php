<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name = "chat")
 * @ORM\MappedSuperclass
 */
class Chat
{
    /**
    * @ORM\Id
    * @ORM\GeneratedValue
    * @ORM\Column(type="integer", name = "id")
    */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, name = "userName")
     */
    private $userName;

    /**
     * @ORM\Column(type="datetime", name = "date")
     */
    private $date;

    /**
     * @ORM\Column(type="string", name = "message")
     */
    private $message;

    public function __construct(string $userName, string $message)
    {
        $this->userName = $userName;
        $this->message = $message;
        $this->date =  new \DateTime('now', new \DateTimeZone(date_default_timezone_get()));
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUserName(): ?string
    {
        return $this->userName;
    }

    public function setUserName(string $userName): self
    {
        $this->userName = $userName;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getData()
    {
        return [
            'id' => $this->id,
            'userName' => $this->userName,
            'date' => $this->date,
            'message' => $this->message
        ];
    }
}
