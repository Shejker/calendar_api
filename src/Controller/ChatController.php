<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/chat")
 */
class ChatController extends AbstractController
{

    /**
     *  Wysłanie wiadomości.
     *
     * @Route("", name="sendMessage", methods={"POST"})
     *
     * @param Request $request Obiekt reprezentujący żądanie HTTP
     */
    public function postChatAction(Request $request)
    {
        return $this->get('chat_handler')->post(json_decode($request->getContent(), true));
    }

    /**
     *  Pobranie wiadomości.
     *
     * @Route("", name="getMessage", methods={"GET"})
     *
     * @param chatId $chatId Identyfikator czatu
     */
    public function getChatAction(Request $request)
    {
        return $this->get('chat_handler')->get($request->query->get('count'));
    }
}
