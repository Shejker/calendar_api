<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/users")
 */
class UsersController extends AbstractController
{
    /**
     *  Dodanie usera.
     *
     * @Route("", name="postUser", methods={"POST"})
     * @IsGranted("ROLE_CALENDAR_ADMIN")
     *
     * @param Request $request Obiekt reprezentujący żądanie HTTP
     */
    public function postUserAction(Request $request)
    {
        return $this->get('user_handler')->post(json_decode($request->getContent(), true));
    }

    /**
     *  Usunięcie usera.
     *
     * @Route("/{userId}", name="deleteUser", methods={"DELETE"})
     * @IsGranted("ROLE_CALENDAR_ADMIN")
     *
     * @param Request $request Obiekt reprezentujący żądanie HTTP
     */
    public function deleteUserAction(int $userId)
    {
        return $this->get('user_handler')->delete($userId);
    }

    /**
     *  Usunięcie usera.
     *
     * @Route("/{userId}/password", name="changeUserPassword", methods={"PUT"})
     *
     * @param Request $request Obiekt reprezentujący żądanie HTTP
     */
    public function changeUserPasswordAction(Request $request, int $userId)
    {
        return $this->get('user_handler')->changePassword(json_decode($request->getContent(), true), $userId);
    }

    /**
     *  Aktualizacja.
     *
     * @Route("/{userId}", name="putUser", methods={"PUT"})
     *
     * @param Request $request Obiekt reprezentujący żądanie HTTP
     * @param userId $userId Identyfikator użytkownika
     */
    public function putUserAction(Request $request, int $userId)
    {
        return $this->get('user_handler')->put(json_decode($request->getContent(), true), $userId);
    }

    /**
     *  Pobranie obiektu zalogowanego użytkownika.
     *
     * @Route("", name="getUser", methods={"GET"})
     *
     * @param Request $request Obiekt reprezentujący żądanie HTTP
     */
    public function getUserAction(Request $request)
    {
        return $this->get('user_handler')->self();
    }

    /**
     *  Pobranie wszystkich użytkowników.
     *
     * @Route("/all", name="getAllUsers", methods={"GET"})
     * @Security("is_granted('ROLE_CALENDAR_ADMIN') or is_granted('ROLE_USER')")
     *
     * @param Request $request Obiekt reprezentujący żądanie HTTP
     */
    public function getAllUsersAction(Request $request)
    {
        return $this->get('user_handler')->getAll();
    }
}
