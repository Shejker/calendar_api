<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/events")
 */
class EventController extends AbstractController
{
    /**
     *  Dodanie usera.
     *
     * @Route("", name="postEvent", methods={"POST"})
     *
     * @Security("is_granted('ROLE_CALENDAR_ADMIN') or is_granted('ROLE_USER')")
     *
     * @param Request $request Obiekt reprezentujący żądanie HTTP
     */
    public function postEventAction(Request $request)
    {
        return $this->get('event_handler')->post(json_decode($request->getContent(), true));
    }

    /**
     *  Ususnięcie zdarzenie.
     *
     * @Route("/{eventId}", name="deleteEvent", methods={"DELETE"})
     *
     * @Security("is_granted('ROLE_CALENDAR_ADMIN') or is_granted('ROLE_USER')")
     *
     * @param userId $eventId Identyfikator użytkownika
     */
    public function deleteEventAction(int $eventId)
    {
        return $this->get('event_handler')->delete($eventId);
    }

    /**
     *  Pobranie zdarzenień użytkownika.
     *
     * @Route("", name="getEvents", methods={"GET"})
     *
     * @param userId $eventId Identyfikator użytkownika
     */
    public function getUserEventAction(Request $request)
    {
        return $this->get('event_handler')->getUserEvents($request->query->get('date'));
    }

    /**
     *  Aktualizuje zdarzenienie.
     *
     * @Route("/{eventId}", name="patchEvents", methods={"patch"})
     *
     *
     * @param userId $eventId Identyfikator użytkownika
     */
    public function patchEventAction(int $eventId, Request $request)
    {
        return $this->get('event_handler')->patch($eventId, json_decode($request->getContent(), true));
    }
}
