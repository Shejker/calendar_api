<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/calendar")
 */
class CalendarController extends AbstractController
{
    /**
     *  Dodanie kalendarza.
     *
     * @Route("", name="postCalendar", methods={"POST"})
     *
     * @param Request $request Obiekt reprezentujący żądanie HTTP
     */
    public function postCalendarAction(Request $request)
    {
        return $this->get('calendar_handler')->post(json_decode($request->getContent(), true));
    }
}
